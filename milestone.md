* Technologies used by checking page names and response headers: PHP/7.2.24 and Apache/2.4.37 (centos)

URL                                       | Input parameters            | Method    | Result
------------------------------------------| --------------------------- | --------- |----------
http://localhost:8080/loginform.php       | username, password, submit  | POST      | ' -> No password parameter provided or it is empty
http://localhost:8080/products/search.php | product, submit             | GET       | ' -> An error occured.
http://localhost:8080/docontact.php       | email, description, submit  | POST      | Your request has been registered. We will contact you soon via e-mail

