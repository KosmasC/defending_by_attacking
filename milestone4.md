# Milestone 4 - Hardening Against Cyber-Attacks

```
kosmas:sqlmap-dev (master)$ ssh root@localhost -p 2222
root@localhost's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Feb 24 17:54:04 2021 from 10.0.2.2
[root@localhost ~]# sudo -u postgres -i
[postgres@localhost ~]$ psql
psql (12.4)
Type "help" for help.

postgres=# SELECT datname FROM pg_dataase;
                            ^
  datname   
------------
 postgres
 userdb
 template1
 template0
 productdb
 purchasedb
(6 rows)

postgres=# CREATE ROLE dbadmin WITH LOGIN PASSWORD '(7G`$2yNf?C1e^_gDFf';
CREATE ROLE
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of 
-----------+------------------------------------------------------------+-----------
 dbadmin   |                                                            | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}

postgres=# GRANT ALL ON DATABASE userdb TO dbadmin;
GRANT
postgres=# GRANT ALL ON DATABASE productdb TO dbadmin;
GRANT
postgres=# GRANT ALL ON DATABASE purchasedb TO dbadmin;
GRANT
postgres=# \c userdb
You are now connected to database "userdb" as user "postgres".
userdb=# GRANT ALL ON TABLE users TO dbadmin;
GRANT
userdb=# \c productdb
You are now connected to database "productdb" as user "postgres".
productdb=# GRANT ALL ON TABLE products TO dbadmin;
GRANT
productdb=# \c purchasedb
You are now connected to database "purchasedb" as user "postgres".
purchasedb=# GRANT ALL ON TABLE purchases TO dbadmin;
GRANT
purchasedb=# 


postgres=# \q
[postgres@localhost ~]$ exit
logout
[root@localhost ~]# vi /var/projects/ecommerce/config.php

[root@localhost ~]# vi /var/lib/pgsql/12/data/postgresql.conf 

[root@localhost ~]# service httpd restart

[root@localhost ~]# service postgresql-12 restart

[root@localhost ~]# tail -f /var/lib/pgsql/12/data/log/postgresql-Mon.log 
2021-03-01 19:37:02.272 EET [13273] LOG:  statement: SELECT name, description FROM products WHERE name = 'test'
2021-03-01 19:37:11.234 EET [13274] LOG:  statement: SELECT name, description FROM products WHERE name = 'test'
2021-03-01 19:37:17.321 EET [13276] LOG:  statement: SELECT count(*) FROM users WHERE username = 'admin' AND password = '922c7b7365d54090b00db14a7bffd1c6'
2021-03-01 19:38:47.191 EET [13253] LOG:  received fast shutdown request
2021-03-01 19:38:47.201 EET [13253] LOG:  aborting any active transactions
2021-03-01 19:38:47.207 EET [13253] LOG:  background worker "logical replication launcher" (PID 13262) exited with exit code 1
2021-03-01 19:38:47.207 EET [13257] LOG:  shutting down
2021-03-01 19:38:47.214 EET [13253] LOG:  database system is shut down
2021-03-01 19:38:47.272 EET [13320] LOG:  database system was shut down at 2021-03-01 19:38:47 EET
2021-03-01 19:38:47.276 EET [13316] LOG:  database system is ready to accept connections
2021-03-01 19:38:56.677 EET [13336] LOG:  connection received: host=::1 port=53890
2021-03-01 19:38:56.678 EET [13336] LOG:  connection authorized: user=dbadmin database=productdb

```

## After hardening try --os-shell

```
kosmas:sqlmap-dev (master)$ python sqlmap.py -u "http://localhost:8080/products/search.php?product=foo" --os-shell
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.5.2.26#dev}
|_ -| . [']     | .'| . |
|___|_  [,]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 22:04:04 /2021-03-01/

[22:04:05] [INFO] resuming back-end DBMS 'postgresql' 
[22:04:05] [INFO] testing connection to the target URL
you have not declared cookie(s), while server wants to set its own ('PHPSESSID=hijb3f2rjkk...lli8hj74de'). Do you want to use those [Y/n] 

sqlmap resumed the following injection point(s) from stored session:
---
Parameter: product (GET)
    Type: stacked queries
    Title: PostgreSQL > 8.1 stacked queries (comment)
    Payload: product=foo';SELECT PG_SLEEP(5)--

    Type: UNION query
    Title: Generic UNION query (NULL) - 2 columns
    Payload: product=foo' UNION ALL SELECT NULL,(CHR(113)||CHR(106)||CHR(112)||CHR(120)||CHR(113))||(CHR(72)||CHR(89)||CHR(82)||CHR(69)||CHR(120)||CHR(82)||CHR(69)||CHR(102)||CHR(109)||CHR(88)||CHR(84)||CHR(99)||CHR(103)||CHR(115)||CHR(76)||CHR(90)||CHR(75)||CHR(71)||CHR(70)||CHR(86)||CHR(68)||CHR(118)||CHR(77)||CHR(120)||CHR(75)||CHR(102)||CHR(87)||CHR(87)||CHR(122)||CHR(80)||CHR(99)||CHR(82)||CHR(111)||CHR(90)||CHR(102)||CHR(111)||CHR(67)||CHR(104)||CHR(90)||CHR(99))||(CHR(113)||CHR(98)||CHR(107)||CHR(120)||CHR(113))-- bMZc
---
[22:04:06] [INFO] the back-end DBMS is PostgreSQL
web server operating system: Linux CentOS 8
web application technology: PHP 7.2.24, Apache 2.4.37, PHP
back-end DBMS: PostgreSQL
[22:04:06] [INFO] fingerprinting the back-end DBMS operating system
[22:04:06] [INFO] the back-end DBMS operating system is Linux
[22:04:06] [INFO] testing if current user is DBA
[22:04:06] [WARNING] something went wrong with full UNION technique (could be because of limitation on retrieved number of entries). Falling back to partial UNION technique
[22:04:06] [WARNING] the SQL query provided does not return any output
[22:04:06] [WARNING] time-based comparison requires larger statistical model, please wait.......................... (done)                              
[22:04:07] [WARNING] it is very important to not stress the network connection during usage of time-based payloads to prevent potential disruptions 

[22:04:07] [WARNING] in case of continuous data retrieval problems you are advised to try a switch '--no-cast' or switch '--hex'
[22:04:07] [INFO] detecting back-end DBMS version from its banner
what is the back-end database management system architecture?
[1] 32-bit (default)
[2] 64-bit
> 

[22:04:21] [CRITICAL] unsupported feature on PostgreSQL 12 (32-bit)

[*] ending @ 22:04:21 /2021-03-01/
```


